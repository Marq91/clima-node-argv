
const axios = require('axios');

const appID = '94661a1eb3537523f425ebd91d960384';

/**
 *
 * Base function to obtain the reques for clima by `latitude` and `longitude`.
 * @param {String} lat latitude params for process
 * @param {String} lng longitude params for process
 */
const getClima = async ( lat, lng ) => {

    const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lng }&appid=${ appID }&units=metric`)

    return response.data.main.temp;

}

module.exports = { getClima }