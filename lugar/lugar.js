const axios = require('axios');

/**
 *
 * @param {String} dir parametro direccion para ser procesada
 */
const getLugarLatLng = async ( dir ) => {

    const encodedUrl = encodeURI( dir );
    
    const instance = axios.create({
      baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${ encodedUrl }`,
      headers: {
        'X-RapidAPI-Key': 'f388994201msh1125d7ab1c4d0b3p10767djsnfd07b42d4cb7'
      }
    });
    
    const resp = await instance.get();

    // Si el results[] viene vacio entonces manejar el error
    if ( resp.data.Results.length == 0 ) {
        throw new Error(`No hay resultados para ${ dir }`)
    }

    // Results[0] por que solo nos interesa la primera posicion
    const data      = resp.data.Results[0];
    const direccion = data.name;
    const lat       = data.lat
    const lng       = data.lon;
    
    return {
        direccion,
        lat,
        lng
    }

}

module.exports = { getLugarLatLng }


